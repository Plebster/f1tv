#!/usr/bin/python3
# from __future__ import annotations
import requests
import json
import time
import sys
import re

from datetime import datetime, timedelta, timezone
from icalendar import Calendar, Event, Alarm
from pydantic import BaseModel, Field, parse_obj_as
from typing import Any, List, Optional

# Model for extracting race details
class Action(BaseModel):
    uri: str

class Container(BaseModel):
     actions: List[Action]

class ResultObj(BaseModel):
    containers: Optional[List[Container]] = None

class RetrieveItems(BaseModel):
     resultObj: ResultObj

class RaceEmfAttributes(BaseModel):
    Meeting_Name: str
    Series: str
    sessionEndDate: int
    sessionStartDate: int

class RaceMetadata(BaseModel):
    emfAttributes: RaceEmfAttributes
    longDescription: str
    title: str

class RaceEvent(BaseModel):
    metadata: RaceMetadata

class RaceContainer(BaseModel):
    eventName: Optional[str] = None
    events: Optional[List[RaceEvent]] = None 


class RaceResultObj(BaseModel):
    containers: List[RaceContainer]


class RaceRetrieveItems(BaseModel):
    resultObj: RaceResultObj


class RaceContainer(BaseModel):
    layout: str
    retrieveItems: RaceRetrieveItems


class RaceResultObj(BaseModel):
    containers: List[RaceContainer]

# Model for extracting race uris
class RaceModel(BaseModel):
    resultObj: Optional[RaceResultObj] = None


class ContainerRaceURI(BaseModel):
     retrieveItems: RetrieveItems

class ResultObjRaceURI(BaseModel):
    containers: List[ContainerRaceURI]

class RaceURI(BaseModel):
     resultObj: Optional[ResultObjRaceURI] = None

# Model for extracting season's uri
class ActionSeasonUri(BaseModel):
    href: str

class ContainerSeasonUri(BaseModel):
    actions: List[ActionSeasonUri]

class ResultObjSeasonUri(BaseModel):
    containers: Optional[List[ContainerSeasonUri]] = None

class WebsiteLinks(BaseModel):
    resultObj: Optional[ResultObjSeasonUri]

# initiate cal
def initiate_calendar():
    cal = Calendar()
    cal.add('X-WR-CALNAME','F1TV Live Broacasts')
    cal.add('X-WR-CALDESC','On track action on F1TV')
    cal.add('X-PUBLISHED-TTL','PT15M')

    return cal
    
def extract_season_id():
    base_uri = "https://f1tv.formula1.com/2.0/A/ENG/WEB_DASH/ALL/MENU/Anonymous/12"
    website_links = requests.get(base_uri).json()
    
    # Parse the JSON response into Pydantic models
    website_links_obj = WebsiteLinks(**website_links)

    # Check if the 'resultObj' field exists in the JSON response
    if website_links_obj.resultObj and website_links_obj.resultObj.containers:
        containers = website_links_obj.resultObj.containers
        
        # Iterate through the containers to find the required information
        for container in containers:
            actions = container.actions
            for action in actions:
                href = action.href
                match = re.search(r'/page/(\d+)/', href)
                if match:
                    season_id = match.group(1)
                    season_uri = "https://f1tv.formula1.com/2.0/A/ENG/WEB_DASH/ALL/PAGE/" + season_id + "/Anonymous/12"
                    return season_uri
                    break  # Stop after finding the first match
            else:
                continue  # Continue to the next container if no match found
            break  # Stop iterating once the required information is found
        else:
            print("No containers found with the required information")
    else:
        print("No containers found in the response")

#  tidy up date to make it usable
def epoch_to_date(epoch):
    date = datetime.fromtimestamp(epoch // 1000)
    return(date)

# retrieve the uris for the full season of f1 races
def get_grand_prix_uris(season_uri):
    season_list = season_uri
    upcoming_races = requests.get(season_list).json()
    outside_containers = (RaceURI(**upcoming_races))
    race_uris = []

    for outside_container in outside_containers.resultObj.containers:
        if outside_container.retrieveItems.resultObj.containers is not None:
            for inside_container in outside_container.retrieveItems.resultObj.containers:
                for action in inside_container.actions:
                    race_uri = 'https://f1tv.formula1.com' + action.uri
                    race_uris.append(race_uri)

    return(race_uris)

# retrieve race details
def get_grand_prix_events(race_uris, cal):
    for race_uri in race_uris:
        # print(race_uri)
        event_details = requests.get(race_uri).json()
        # event_details = json.load(race_uris)
        outside_containers = (RaceModel(**event_details))
        for containers in outside_containers.resultObj.containers:
            if containers.layout == 'interactive_schedule':
                for events_containers in containers.retrieveItems.resultObj.containers:
                    if events_containers.eventName == 'ALL':
                        for event in events_containers.events:
                            event_name = event.metadata.emfAttributes.Meeting_Name
                            series = event.metadata.emfAttributes.Series.title()

                            if '#' in event.metadata.longDescription and 'Show' not in event.metadata.title:
                                session = event.metadata.title.split('- ', 1)[1]
                            elif '#' in event.metadata.longDescription and 'Show' in event.metadata.title:
                                session = event.metadata.title.split('- ', 1)[0]
                                series = 'Formula 1'
                            else:
                                session = event.metadata.longDescription
                            sessionStartDate = epoch_to_date(event.metadata.emfAttributes.sessionStartDate)
                            sessionEndDate = epoch_to_date(event.metadata.emfAttributes.sessionEndDate)
                            
                            write_cal_event(sessionStartDate, sessionEndDate, event_name, series, session, cal)


def write_cal_event(sessionStartDate, sessionEndDate, event_name, series, session, cal):
    uid = sessionStartDate.strftime('%Y%m%d%H%M%S')
    event = Event()
    event.add('uid', uid)
    event.add('summary', '{} - {} - {}'.format(series, session, event_name))
    
    # Convert sessionStartDate and sessionEndDate to datetime objects if they are not already
    if not isinstance(sessionStartDate, datetime):
        sessionStartDate = datetime.utcfromtimestamp(sessionStartDate)
    if not isinstance(sessionEndDate, datetime):
        sessionEndDate = datetime.utcfromtimestamp(sessionEndDate)
    
    # Convert datetime objects to UTC
    sessionStartDate = sessionStartDate.astimezone(timezone.utc)
    sessionEndDate = sessionEndDate.astimezone(timezone.utc)
    
    # Use datetime objects directly with timezone information
    event.add('dtstart', sessionStartDate)
    event.add('dtend', sessionEndDate)

    alarm = Alarm()
    alarm.add('X-WR-ALARMUID', uid + 'AL')
    alarm.add('uid', uid + 'AL')
    alarm.add('description', 'Reminder')
    alarm.add('TRIGGER', timedelta(minutes=-5))
    alarm.add('ACTION', 'AUDIO')
    alarm.add('ATTACH;VALUE=URI','Chord')
    event.add_component(alarm)

    cal.add_component(event)

    return cal








# write cal
def write_cal(cal):
    f = open('/cal/f1tv.ics', 'wb')
    f.write(cal.to_ical())
    f.close()
       
if __name__ == "__main__":
    # initiate cal
    cal = initiate_calendar()

    # extract season id
    season_uri = extract_season_id()
    
    # get uris for grand prix
    race_uris = get_grand_prix_uris(season_uri)

    # get events for grand prix
    get_grand_prix_events(race_uris, cal)
    
    # write cal
    write_cal(cal)

    # print(cal)

    
