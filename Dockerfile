FROM python:3

# ENV TZ="Europe/London"

ENV TZ="UTC"

WORKDIR /app

COPY f1tv.py /app

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

CMD [ "python", "./f1tv.py" ]

# docker run --rm $PWD:/cal f1tv