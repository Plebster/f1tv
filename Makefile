TERRAFORM_URL = https://releases.hashicorp.com/terraform/0.13.5/
TERRAFORM_AMD_FILE = terraform_0.13.5_linux_amd64.zip
TERRAFORM_ARM_FILE = terraform_0.13.5_linux_arm64.zip

build-amd64:
	docker buildx build --push --platform linux/amd64 --build-arg TERRAFORM_URL=$(TERRAFORM_URL) --build-arg TERRAFORM_FILE=$(TERRAFORM_AMD_FILE) --tag "$(IMAGE):$(TAG)-amd64" ./

build-arm64:
	docker buildx build --push --platform linux/arm64 --build-arg TERRAFORM_URL=$(TERRAFORM_URL) --build-arg TERRAFORM_FILE=$(TERRAFORM_ARM_FILE) --tag "$(IMAGE):$(TAG)-arm64" ./

build-manifest:
	docker manifest create $(IMAGE):$(TAG) "$(IMAGE):$(TAG)-amd64" "$(IMAGE):$(TAG)-arm64"
	docker manifest push $(IMAGE):$(TAG)
